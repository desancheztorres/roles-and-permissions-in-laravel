<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

Route::get('/', function (Request $request) {
    $user = $request->user();

    $user->withdrawPermissionTo(['delete users', 'edit posts']);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'role:admin'], function () {

    Route::group(['middleware' => 'role:admin,delete users'], function () {
        Route::get('/admin/users', function() {
            return 'Delete users in admin panel';
        });
    });

    Route::get('/admin', function() {
        return 'Admin panel';
    });
});
